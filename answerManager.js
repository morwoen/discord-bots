function AnswerManager() {}

AnswerManager.isCalled = false;
AnswerManager.command = [];

AnswerManager.Reset = function() {
	AnswerManager.isCalled = false;
	AnswerManager.command = [];
}

AnswerManager.Respond = function(msg) {
	if((msg.isMentioned(gri6a.user) || msg.content.startsWith(gManager.name) || _.some(gManager.aliases, function(alias){ return msg.content.startsWith(alias) })) && AnswerManager.isCalled === false) {
		AnswerManager.isCalled = true;
		var input = msg.content.split(", ");
		input.shift();
		if(input.length > 0) {
			AnswerManager.command = input;
			AnswerManager.Answer(msg);
		} else {
			msg.channel.send("Кажи Баце?");
			var listen = new Discord.MessageCollector(msg.channel, m => m.author.id === msg.author.id, {time: 10000});
			listen.on('collect', function(msg) {
				listen.stop();
				var input = msg.content.split(", ");
				AnswerManager.command = input;
				AnswerManager.Answer(msg);
			});
			listen.on('end', function() {
				if(listen._timeout._called) {
					msg.channel.send("Не мога да те чакам цял ден Баце, заеби.");
					AnswerManager.Reset();
				}
			});
		}
	} else {
		return;
	}
}

AnswerManager.Answer = function(msg) {
	var command = _.find(msg.channel.commands, function(command) {
		return command.name == AnswerManager.command[0];
	});
	if(command) {
		AnswerManager.Execute(command, msg, 0);
	} else {
		msg.channel.send("Нема такава команда, Баце.");
		AnswerManager.isCalled = false;
	}
}

AnswerManager.Execute = function(command, message, iteration) {
	if(AnswerManager.command[iteration]) {
		if(command.prompts.length > iteration) {
			AnswerManager.Execute(command, message, iteration + 1);
		} else {
			AnswerManager.HandleReply(command, message);
		}
	} else {
		message.channel.send(command.prompts[iteration - 1]);
		var listen = new Discord.MessageCollector(message.channel, m => m.author.id === message.author.id, {time: 10000});
		listen.on('collect', function(msg) {
			listen.stop();
			AnswerManager.command[iteration] = msg.content;
			AnswerManager.Execute(command, message, iteration);
		});
		listen.on('end', function() {
			if(listen._timeout._called) {
				message.channel.send("Не мога да те чакам цял ден Баце, заеби.");
				AnswerManager.Reset();
			}
		});
	}
}

AnswerManager.HandleReply = function(command, message) {
	if(command.reply) {
		message.author.send(command.reply).then(function(result) {
			if(command.pin) {
				result.pin();
			}
		});
		AnswerManager.Reset();
		return;
	}
	if(command.message) {
		message.channel.send(command.message).then(function(result) {
			if(command.pin) {
				result.pin();
			}
		});
		AnswerManager.Reset();
		return;
	}
	if(command.execution) {
		AnswerManager.command.shift();
		Promise.resolve().then(function() {
			return command.execution(AnswerManager.command, message);
		}).then(function(result) {
			message.channel.send(result).then(function(result) {
				if(command.pin) {
					result.pin();
				}
				if(command.react) {
					Promise.map(command.react, function(reaction) {
						result.react(reaction);
					}, {concurrency: 1}).then(function() {
						result.awaitReactions(function(react) {
							var user = react.users.array()[react.users.array().length - 1];
							if(user.id != gri6a.user.id) {
								var index = command.react.indexOf(react._emoji.id);
								if(index != -1) {
									command.reactions[index](result, user, command);
								}
							}
						});
					});
				}
			});
			AnswerManager.Reset();
			return;
		});
	}
}

module.exports = AnswerManager;