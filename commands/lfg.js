module.exports = [
	{
		name: "add",
		prompts: ["Дай име на партито.", "Коя игра ще цъкаш, Баце?", "Колко народ ти трябва, Баце?"],
		execution: function(args, message) {
			var cap = parseInt(args[2]);
			if(cap < 2 || isNaN(cap)) {
				cap = 2;
			}
			var embed = new Discord.RichEmbed()
				.setTitle(args[0])
				.setColor("GREEN")
				.setFooter(`${args[0]} играят: ${args[1]}`)
				.setTimestamp()
				// .setThumbnail("https://ih0.redbubble.net/image.368195583.8025/raf,750x1000,075,t,fafafa:ca443f4786.u2.jpg")
				.setAuthor(message.author.username, message.author.avatarURL)
				.addField("Игра:", args[1])
				.addField("Парти:", [message.author.username], true)
				.addField("Хора:", 1, true)
				.addField("Капацитет:", cap, true);
			return embed;
		},
		pin: true,
		react: [
			"452110953903947787",
			"452110954164125696",
			"452110953861873675"
		],
		reactions: [
			function(message, user, command) {
				var oldEmbed = message.embeds[0];
				if(oldEmbed.fields[1]["value"].includes(user.username)) {
					message.clearReactions().then(function() {
						_.each(command.react, function(reaction) {
							message.react(reaction);
						});
					});
					return;
				} else {
					if(parseInt(oldEmbed.fields[2]["value"]) < parseInt(oldEmbed.fields[3]["value"])) {
						message.edit("", new Discord.RichEmbed()
							.setTitle(oldEmbed.title)
							.setColor(oldEmbed.color)
							.setFooter(oldEmbed.footer["text"])
							.setTimestamp()
							// .setThumbnail("https://ih0.redbubble.net/image.368195583.8025/raf,750x1000,075,t,fafafa:ca443f4786.u2.jpg")
							.setAuthor(oldEmbed.author["name"], oldEmbed.author["iconURL"])
							.addField("Игра:", oldEmbed.fields[0]["value"])
							.addField("Парти:", oldEmbed.fields[1]["value"] += `, ${user.username}`, true)
							.addField("Хора:", parseInt(oldEmbed.fields[2]["value"]) + 1, true)
							.addField("Капацитет:", oldEmbed.fields[3]["value"], true)
						).then(function() {
							message.clearReactions().then(function() {
								_.each(command.react, function(reaction) {
									message.react(reaction);
								});
							});
						});
						return;
					} else {
						console.log("Full");
						return;
					}
				}
			},
			function(message, user, command) {
				var oldEmbed = message.embeds[0];
				if(!oldEmbed.fields[1]["value"].includes(user.username)) {
					message.clearReactions().then(function() {
						_.each(command.react, function(reaction) {
							message.react(reaction);
						});
					});
					return;
				} else if(parseInt(oldEmbed.fields[2]["value"]) < 2) {
					var array = oldEmbed.fields[1]["value"].split(", ");
					message.edit("", new Discord.RichEmbed()
						.setTitle("Партито е затворено")
						.setColor("DEFAULT")
						.setFooter("Партито е затворено")
						.setTimestamp()
						// .setThumbnail("https://ih0.redbubble.net/image.368195583.8025/raf,750x1000,075,t,fafafa:ca443f4786.u2.jpg")
						.setAuthor(oldEmbed.author["name"], oldEmbed.author["iconURL"])
						.addField("Игра:", oldEmbed.fields[0]["value"])
						.addField("Парти:", "Празно", true)
						.addField("Хора:", 0, true)
						.addField("Капацитет:", 0, true)
					).then(function() {
						message.clearReactions().then(function() {
							message.unpin();
						});
					});
					return;
				} else {
					var array = oldEmbed.fields[1]["value"].split(", ");
					array.splice(array.indexOf(user.username), 1);
					message.edit("", new Discord.RichEmbed()
						.setTitle(oldEmbed.title)
						.setColor(oldEmbed.color)
						.setFooter(oldEmbed.footer["text"])
						.setTimestamp()
						// .setThumbnail("https://ih0.redbubble.net/image.368195583.8025/raf,750x1000,075,t,fafafa:ca443f4786.u2.jpg")
						.setAuthor(oldEmbed.author["name"], oldEmbed.author["iconURL"])
						.addField("Игра:", oldEmbed.fields[0]["value"])
						.addField("Парти:", array, true)
						.addField("Хора:", parseInt(oldEmbed.fields[2]["value"]) - 1, true)
						.addField("Капацитет:", oldEmbed.fields[3]["value"], true)
					).then(function() {
						message.clearReactions().then(function() {
							_.each(command.react, function(reaction) {
								message.react(reaction);
							});
						});
					});
					return;
				}
			},
			function(message, user, command) {
				var oldEmbed = message.embeds[0];
				if(oldEmbed.author["name"] == user.username) {
					message.edit("", new Discord.RichEmbed()
						.setTitle("Партито е затворено")
						.setColor("DEFAULT")
						.setFooter("Партито е затворено")
						.setTimestamp()
						// .setThumbnail("https://ih0.redbubble.net/image.368195583.8025/raf,750x1000,075,t,fafafa:ca443f4786.u2.jpg")
						.setAuthor(oldEmbed.author["name"], oldEmbed.author["iconURL"])
						.addField("Игра:", oldEmbed.fields[0]["value"])
						.addField("Парти:", "Празно", true)
						.addField("Хора:", 0, true)
						.addField("Капацитет:", 0, true)
					).then(function() {
						message.clearReactions().then(function() {
							message.unpin();
						});
					});
					return;
				} else {
					message.clearReactions().then(function() {
						_.each(command.react, function(reaction) {
							message.react(reaction);
						});
					});
					return;
				}
			}
		]
	}
]