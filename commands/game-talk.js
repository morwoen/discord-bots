module.exports = [
	{
		name: "list",
		prompts: [],
		execution: function(args, message) {
			var roles = [];
			roles = _.map(_.filter(message.guild.roles.array(), function(role) {
				return role.name.includes("Squad");
			}), function(role) {
				return role.name;
			});
			var embed = new Discord.RichEmbed()
				.setTitle("Списък с отряди")
				.setDescription(roles.join('\n'))
				.setColor("DARK_GOLD");
			return embed; 
		}
	},
	{
		name: "join",
		prompts: ["Към кой отряд искаш да се присъединиш, Баце?"],
		execution: function(args, message) {
			var role = message.guild.roles.find("name", args[0]);
			if(!role) {
				return "Няма такъв отряд, Баце."
			}
			if(message.member.roles.has(role.id)) {
				return "Вече си в тая група, Баце.";
			} else {
				return message.member.addRole(role)
					.then(function() {
						return "Готов си, Баце.";
					})
					.catch(function() {
						return "Нещо някъде си еба майката, Баце.";
					});
			}
		}
	},
	{
		name: "leave",
		prompts: ["От кой отряд искаш да се махнеш, Баце?"],
		execution: function(args, message) {
			var role = message.guild.roles.find("name", args[0]);
			if(!role) {
				return "Няма такъв отряд, Баце."
			}
			if(!message.member.roles.has(role.id)) {
				return "Не си в този отряд, Баце.";
			} else {
				return message.member.removeRole(role)
					.then(function() {
						return "Готов си, Баце.";
					})
					.catch(function() {
						return "Нещо някъде си еба майката, Баце.";
					});
			}
		}
	}
]