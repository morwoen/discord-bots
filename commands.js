var commands = {};

_.each(Constants.Channels, function(channel) {
	commands[channel.id] = [];
});

GLOBAL.sc = mongoose.model("simpleCommands", { command: String, help: String, reply: String, replyCount: Number });
sc.find().then(function(answer){
	commands[Constants.Channels.everywhere.id] = _.union(commands[Constants.Channels.everywhere.id], answer);
});

fs.readdir('commands', function(err, files) {
	_.each(files, function(file) {
		require("./commands/" + file)(commands);
	});
});

module.exports = commands;
//TODO Всичко отдолу трябва да стане като отгоре
var CommandsTODO = {
	renameCommand: {
		name: "newName",
		help: "Add new condition to command",
		reply: "Вече ке викам на {{name}}, {{newCondition}}"
	},
	forgetCommand: {
		name: "removeName",
		help: "Remove condition from command",
		reply: "Хубу, ке си му викам {{name}}"
	},
	titsList: {
		name: "list",
		help: "List 4chan threads in watchlist",
		channel: Constants.Channels.nsfw,
		reply: "У торбътъ с коврье: {{threads}}"
	},
	titsAdd: {
		name: "add",
		help: "Add new 4chan thread to watchlist",
		channel: Constants.Channels.nsfw,
		reply: "Ша го зема и ша ти ареса"
	},
	titsCount: {
		name: "count",
		help: "Display current image count in watchlist",
		channel: Constants.Channels.nsfw,
		reply: "Епа, около {{count}} коврье имаме"
	},
	titsPull: {
		name: "pull",
		help: "Post image from watchlist",
		channel: Constants.Channels.nsfw,
		reply: "{{image}}"
	},
	titsPour: {
		name: "pour",
		help: "Post 10 images from watchlist",
		channel: Constants.Channels.nsfw,
		reply: "{{image}}"
	},
	addChannel: {
		name: "add",
		help: "Create new game voice channel",
		channel: Constants.Channels.lfg,
		reply: "Ша ти прасна канал баце"
	},
	joinChannel: {
		name: "join",
		help: "Join game voice channel",
		channel: Constants.Channels.lfg,
		reply: "Оди при оня пустиняк баце"
	},
	lfg: {
		name: "lfg",
		help: "Sets lfg",
		channel: Constants.Channels.lfg,
		reply: "Ща споменавам къв си пустиняк"
	},
	nolfg: {
		name: "stop",
		help: "Removes lfg status",
		channel: Constants.Channels.lfg,
		reply: "Епа поне да не шплякаш сам, че е тъжно баце"
	},
	saveCoords: {
		name: "save",
		help: "Save coordinates",
		channel: Constants.Channels.minecraft,
		reply: "Те там ке биде България"
	},
	listCoords: {
		name: "list",
		help: "Lists all saved coordinates",
		channel: Constants.Channels.minecraft,
		reply: "{{coords}}"
	},
	updateNews: {
		name: "update",
		help: "Set server status",
		channel: Constants.Channels.minecraft,
		reply: "Ша та запомнъ"
	},
	startServer: {
		name: "start",
		help: "Starts the server",
		channel: Constants.Channels.minecraft,
		reply: "Па ша го пусна баце"
	},
	stopServer: {
		name: "stop",
		help: "Stops the server",
		channel: Constants.Channels.minecraft,
		reply: "Ша са възспра баце"
	}
};